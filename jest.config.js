module.exports = {
  setupFiles: ["<rootDir>/setuptests.ts"],
  testMatch: ["<rootDir>/src/test/**/*.test.[jt]s?(x)"],
  collectCoverage: true
};
