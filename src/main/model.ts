import { Task } from "./data/Task";
import { State } from "./state";

export class Model {
  private state;
  constructor(state: State) {
    this.state = state;
  }
  public accept(proposal): Promise<State> {
    return this[proposal.name](proposal.data);
  }
  private createTask(): Promise<State> {
    return this.updateState(newState => {
      newState.tasks.push(
        new Task(this.state.tasks.length.toString(), this.state.newTask)
      );
      newState.newTask = "";
    });
  }
  private editNewTask(desc): Promise<State> {
    return this.updateState(newState => (newState.newTask = desc));
  }
  private updateState(update): Promise<State> {
    const newState = this.state.copy();
    update(newState);
    this.state = newState;
    return Promise.resolve(newState);
  }
}
