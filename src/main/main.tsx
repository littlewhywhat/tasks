import * as React from "react";
import ReactDom from "react-dom";
import { App } from "./components/App";
import { Task } from "./data/Task";
import { Model } from "./model";
import { State } from "./state";

const initialState = new State([
  new Task("0", "buy some milk"),
  new Task("1", "go out with my dog")
]);
const model = new Model(initialState);
ReactDom.render(
  <App initialState={initialState} model={model} />,
  document.getElementById("app")
);
