export const createLoop = (render, model) => {
  const loop = async (actionPromise: Promise<any>) => {
    const proposal = await actionPromise;
    const newState = await model.accept(proposal);
    render(newState);
  };
  return loop;
};
