export class Task {
  public id: string;
  public desc: string;
  constructor(id: string, desc: string) {
    this.id = id;
    this.desc = desc;
  }
}
