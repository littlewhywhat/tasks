import * as React from "react";
import { createLoop } from "../loop";

import { Tasks } from "./Tasks";

export const App = ({ initialState, model }) => {
  const [state, setState] = React.useState(initialState);

  const loop = createLoop(setState, model);

  return Tasks(state, loop);
};
