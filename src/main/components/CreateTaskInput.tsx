import * as React from "react";
import { CreateTaskAction } from "../actions/CreateTaskAction";
import { EditNewTaskAction } from "../actions/EditNewTaskAction";

export const CreateTaskInput = (desc, loop) => {
  return (
    <form
      onSubmit={e => {
        loop(CreateTaskAction());
        e.preventDefault();
      }}
    >
      <input
        id="new-task-input"
        type="text"
        onChange={e => loop(EditNewTaskAction(e))}
        value={desc}
      ></input>
    </form>
  );
};
