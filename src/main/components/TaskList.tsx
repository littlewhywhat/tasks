import * as React from "react";
import { Task } from "../data/Task";

export let TaskList = (tasks: Task[]) => {
  const tasksItems = tasks.map(task => <li key={task.id}>{task.desc}</li>);
  return <ul id="tasks-list">{tasksItems}</ul>;
};
