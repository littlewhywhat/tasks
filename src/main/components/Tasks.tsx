import * as React from "react";
import { State } from "../state";
import { CreateTaskInput } from "./CreateTaskInput";
import { TaskList } from "./TaskList";

export const Tasks = (state: State, loop) => {
  return (
    <div>
      {CreateTaskInput(state.newTask, loop)} {TaskList(state.tasks)}
    </div>
  );
};
