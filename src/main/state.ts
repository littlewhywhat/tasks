import { Task } from "./data/Task";

export class State {
  public tasks: Task[];
  public newTask: string;
  constructor(tasks = [], newTask = "") {
    this.tasks = tasks;
    this.newTask = newTask;
  }
  public copy() {
    const newTaskCopy = (" " + this.newTask).slice(1);
    return new State(this.tasks.slice(), newTaskCopy);
  }
}
