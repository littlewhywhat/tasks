import { CreateTaskProposal } from "../proposals/CreateTaskProposal";

export let CreateTaskAction = async (): Promise<any> => {
  return Promise.resolve(new CreateTaskProposal());
};
