import { EditNewTaskProposal } from "../proposals/EditNewTaskProposal";

export let EditNewTaskAction = async (e): Promise<any> => {
  return Promise.resolve(new EditNewTaskProposal(e.target.value));
};
