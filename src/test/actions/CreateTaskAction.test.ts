import { CreateTaskAction } from "../../main/actions/CreateTaskAction";
import { CreateTaskProposal } from "../../main/proposals/CreateTaskProposal";

describe("CreateTaskAction", () => {
  it("resolves to create task proposal", async () => {
    const proposal = await CreateTaskAction();
    expect(proposal).not.toBeNull();
    expect(proposal).toBeInstanceOf(CreateTaskProposal);
  });
});
