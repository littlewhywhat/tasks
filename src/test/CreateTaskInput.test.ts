import { shallow } from "enzyme";
import { CreateTaskInput } from "../main/components/CreateTaskInput";

const TASK_DESC = "task 1";
const LOOP_MOCK = jest.fn();
const EVENT = {
  preventDefault: jest.fn(),
  target: 1
};

describe("CreateTaskInput", () => {
  let createTaskInput = null;
  beforeEach(() => {
    createTaskInput = shallow(CreateTaskInput(TASK_DESC, LOOP_MOCK));
  });

  describe("input", () => {
    let input = null;
    beforeEach(() => {
      jest.clearAllMocks();
      input = createTaskInput.find("input");
    });
    it("exists", () => {
      expect(input).not.toBeNull();
    });
    it("displays description", () => {
      expect(input.props().value).toEqual(TASK_DESC);
    });
    describe("on change", () => {
      beforeEach(() => {
        input.simulate("change", EVENT);
      });
      it("calls loop with EditNewTaskAction result", () => {
        expect(LOOP_MOCK).toBeCalledTimes(1);
      });
    });
    describe("on submit", () => {
      beforeEach(() => {
        createTaskInput.find("form").simulate("submit", EVENT);
      });
      it("calls loop with CreateTaskAction result", () => {
        expect(LOOP_MOCK).toBeCalledTimes(1);
      });
      it("prevents default for an event", () => {
        expect(EVENT.preventDefault).toBeCalledTimes(1);
      });
    });
  });
});
