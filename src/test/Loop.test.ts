import { createLoop } from "../main/loop";

const PROPOSAL_1 = {
  create: 1
};
const STATE = {
  counter: 1
};
const RENDER_MOCK = jest.fn();
const MODEL_MOCK = {
  accept: jest.fn()
};
const RESOLVED_ACTION_PROMISE_1 = Promise.resolve(PROPOSAL_1);

describe("Loop", () => {
  let loop = null;
  beforeEach(() => {
    jest.clearAllMocks();
    MODEL_MOCK.accept.mockResolvedValue(STATE);
    loop = createLoop(RENDER_MOCK, MODEL_MOCK);
  });
  it("can be created", () => {
    expect(loop).not.toBeNull();
  });
  describe("when it executes", () => {
    beforeEach(() => {
      loop(RESOLVED_ACTION_PROMISE_1);
    });
    it("renders a correct state", () => {
      expect(RENDER_MOCK).toBeCalledTimes(1);
      expect(RENDER_MOCK).toBeCalledWith(STATE);
    });
    it("proposes correctly to model", () => {
      expect(MODEL_MOCK.accept).toBeCalledTimes(1);
      expect(MODEL_MOCK.accept).toBeCalledWith(PROPOSAL_1);
    });
  });
});
