import { shallow } from "enzyme";
import { Tasks } from "../main/components/Tasks";
import { Task } from "../main/data/Task";
import { State } from "../main/state";

const TASK_1 = new Task("1", "task 1");

describe("Tasks", () => {
  const tasks = shallow(Tasks(new State([TASK_1]), jest.fn()));

  it("renders the list", () => {
    const ul = tasks.find("ul");
    expect(ul.exists()).toBe(true);
    expect(ul.children.length).toEqual(1);
  });
  it("renders the input", () => {
    expect(tasks.find("form").exists()).toBe(true);
  });
  it("renders the task", () => {
    const li = tasks.find("li");
    expect(li.key()).toEqual(TASK_1.id);
    expect(li.text()).toEqual(TASK_1.desc);
  });
});
