import { Task } from "../main/data/Task";
import { Model } from "../main/model";
import { CreateTaskProposal } from "../main/proposals/CreateTaskProposal";
import { EditNewTaskProposal } from "../main/proposals/EditNewTaskProposal";
import { State } from "../main/state";

const INIT_STATE = new State();

const TASK_1_DESC = "task 1";
const TASK_2_DESC = "task 2";
const TASK_1 = new Task("0", TASK_1_DESC);
const TASK_2 = new Task("1", TASK_2_DESC);

describe("Model", () => {
  let model = null;
  beforeEach(() => {
    model = new Model(INIT_STATE);
  });
  it("can be created", () => {
    expect(model).not.toBeNull();
  });
  describe("when is proposed to edit new task", () => {
    let state = null;
    beforeEach(async () => {
      state = await model.accept(new EditNewTaskProposal(TASK_1_DESC));
    });
    it("creates a new state with new description", () => {
      expect(state).not.toBeNull();
      expect(state).not.toBe(INIT_STATE);
      expect(state.newTask).toEqual(TASK_1_DESC);
    });
    describe("and then is proposed to create a new task", () => {
      let prevState = null;
      beforeEach(async () => {
        prevState = state;
        state = await model.accept(new CreateTaskProposal());
      });
      it("creates a new state with an added task", () => {
        expect(state).not.toBeNull();
        expect(state).not.toBe(prevState);
        const tasks = prevState.tasks.slice();
        tasks.push(TASK_1);
        expect(state.tasks).toEqual(tasks);
      });
      it("creates a new state with clean new task", () => {
        expect(state.newTask).toEqual("");
      });
      describe("and then is proposed to create another task", () => {
        beforeEach(async () => {
          prevState = state;
          await model.accept(new EditNewTaskProposal(TASK_2_DESC));
          state = await model.accept(new CreateTaskProposal());
        });
        it("creates a new state with an added task", () => {
          const tasks = prevState.tasks.slice();
          tasks.push(TASK_2);
          expect(state.tasks).toEqual(tasks);
        });
      });
    });
  });
});
