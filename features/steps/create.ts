import { Given, Then, When } from "cucumber";
import { Selector as NativeSelector } from "testcafe";

const select = (input, t) => {
  return NativeSelector(input).with({ boundTestRun: t });
};

const createTask = async (desc, t) => {
  await t.typeText(select("#new-task-input", t), desc).pressKey("enter");
};

const checkTask = async (desc, t, i) => {
  await t.expect(select("#tasks-list", t).child(i).innerText).eql(desc);
};

Given("a user views the list of tasks", async t => {});

When("the user creates a new task", async t => {
  t.fixtureCtx.task = "Buy milk";
  await createTask(t.fixtureCtx.task, t);
});

When("the user creates two tasks", async t => {
  t.fixtureCtx.tasks = ["Buy milk", "Go out with my dog"];
  for (const task of t.fixtureCtx.tasks) {
    await createTask(task, t);
  }
});

Then("the user can see the new task appended to the list", async t => {
  await checkTask(t.fixtureCtx.task, t, -1);
});

Then("the user can see both tasks appended to the list", async t => {
  let index = -1;
  const tasks = t.fixtureCtx.tasks;
  for (let i = tasks.length - 1; i >= 0; i--) {
    await checkTask(tasks[i], t, index);
    index--;
  }
});
