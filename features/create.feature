@dashboard
Feature: Task creation

   Scenario: a user can create a task
      Given a user views the list of tasks
      When the user creates a new task
      Then the user can see the new task appended to the list

   Scenario: a user can create several tasks
      Given a user views the list of tasks
      When the user creates two tasks
      Then the user can see both tasks appended to the list
