import { Before } from "cucumber";
import { config } from "./config";

Before("@dashboard", async t => {
  await t.navigateTo(`http://localhost:${config.port}`);
});
