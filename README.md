## Tasks

Tasks is a simple to do list that is developed for educational purposes.

It follows a SAM pattern (http://sam.js.org).

It leverages recent technologies like React, Typescript, Cucumber, TestCafe and Jest.

#### Build and run server

```
yarn
yarn test
yarn start
```

#### Run e2e

```
yarn e2e
```
